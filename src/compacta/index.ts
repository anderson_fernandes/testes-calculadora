import "chromedriver";

import webdriver, { Builder, By, until, WebElement } from "selenium-webdriver";
import { Options } from "selenium-webdriver/chrome";

const url = "http://3.20.29.193/login.php?return=%2Findex.php";

async function teste(ids: string[]): Promise<string> {
  const options = new Options();

  options.addArguments("--no-sandbox");

  options.addArguments(
    "user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36"
  );

  options.setUserPreferences({
    "download.prompt_for_download": false,

    "download.directory_upgrade": true,

    "safebrowsing.enabled": true,
  });

  const driver = await new Builder()

    .withCapabilities(webdriver.Capabilities.chrome())

    // .forBrowser('chrome')

    .setChromeOptions(options)

    .build();

  let exists = false;

  while (!exists) {
    try {
      await driver.get(url);

      exists = true;
    } catch (error) {
      console.log(error);

      await driver.sleep(1000);
    }
  }

  let filledEmail = false;

  while (!filledEmail) {
    try {
      await (
        await driver.findElement(
          By.xpath('//*[@id="login"]/fieldset/div[1]/input')
        )
      ).sendKeys("tulio.pantuza@precato.com.br");

      filledEmail = true;
    } catch (error) {
      console.log(error);

      await driver.sleep(1000);
    }
  }

  let filledPassword = false;

  while (!filledPassword) {
    try {
      await (
        await driver.findElement(
          By.xpath('//*[@id="login"]/fieldset/div[2]/input')
        )
      ).sendKeys("Precato15");

      filledPassword = true;
    } catch (error) {
      console.log(error);

      await driver.sleep(1000);
    }
  }

  let buttonClick = false;

  while (!buttonClick) {
    try {
      await (
        await driver.findElement(By.xpath('//*[@id="login"]/fieldset/input'))
      ).click();

      await driver.sleep(5000);

      buttonClick = true;
    } catch (error) {
      console.log(error);
    }
  }

  let calculator = false;

  while (!calculator) {
    try {
      await (
        await driver.findElement(
          By.xpath('//*[@id="side-menu"]/li[1]/ul/li[18]/a')
        )
      ).click();

      calculator = true;
    } catch (error) {
      console.log(error);

      await driver.sleep(1000);
    }
  }

  for (const id of ids) {
    let hasId1 = false;

    while (!hasId1) {
      try {
        await driver.wait(
          until.elementLocated(By.xpath('//*[@id="idpipedrive"]')),

          60000
        );

        await (
          await driver.findElement(By.xpath('//*[@id="idpipedrive"]'))
        ).clear();

        for (const n of id) {
          await (
            await driver.findElement(By.xpath('//*[@id="idpipedrive"]'))
          ).sendKeys(n);
        }

        hasId1 = true;

        await driver.sleep(5000);
      } catch (err) {
        console.log(err);

        await driver.sleep(1000);
      }
    }

    await driver.sleep(4000);

    await (await driver.findElement(By.xpath('//*[@id="percentual"]'))).clear();

    let percentual = false;

    while (!percentual) {
      try {
        await (
          await driver.findElement(By.xpath('//*[@id="percentual"]'))
        ).sendKeys("10");

        percentual = true;
      } catch (error) {
        console.log(error);

        await driver.sleep(1000);
      }
    }

    await (await driver.findElement(By.xpath('//*[@id="honorario"]'))).clear();

    let honorario = false;

    while (!honorario) {
      try {
        await (
          await driver.findElement(By.xpath('//*[@id="honorario"]'))
        ).sendKeys("8");

        honorario = true;
      } catch (error) {
        console.log(error);

        await driver.sleep(1000);
      }
    }

    await driver.sleep(5000);

    driver
      .findElement(By.xpath('//*[@id="honorario"]'))
      .getAttribute("value")
      .then(function (result) {
        console.log(result);
      });

    let buttonCalcular = false;

    while (!buttonCalcular) {
      try {
        await (
          await driver.findElement(By.xpath('//*[@id="btnCalcular"]'))
        ).click();

        buttonCalcular = true;
      } catch (error) {
        console.log(error);

        await driver.sleep(1000);
      }
    }

    await driver.sleep(3000);
  }

  return "success";
}

export { teste };
