import "chromedriver";
import fs from "fs";
import webdriver, { Builder, By, until } from "selenium-webdriver";
import { Options } from "selenium-webdriver/chrome";

interface IData {
  id: string;
  name: string;
  cpf: string;
  birth: string;
  age: string;
  trf: string;
  date: string;
  cnx: string;
  income: string;
  init: string;
  to: string;
  reason: string;
  stick: string;
  lawyer: string;
  character: string;
  fees: string;
  percentage: string;
  data_base: string;
  number: string;
  valor_oficio: string;
  value_update: string;
  valueFees: string;
  ir: string;
  pss: string;
  valuePrecatory: string;
  valueProposal: string;
}

const url = "http://localhost:3000/";

async function teste(ids: string[]): Promise<string> {
  const options = new Options();
  options.addArguments("--no-sandbox");

  options.addArguments(
    "user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36"
  );

  options.setUserPreferences({
    "download.prompt_for_download": false,

    "download.directory_upgrade": true,

    "safebrowsing.enabled": true,
  });

  const driver = await new Builder()

    .withCapabilities(webdriver.Capabilities.chrome())

    // .forBrowser('chrome')

    .setChromeOptions(options)

    .build();

  let exists = false;

  while (!exists) {
    try {
      await driver.get(url);

      exists = true;
    } catch (error) {
      console.log(error);
      await driver.sleep(1000);
    }
  }

  let filledEmail = false;

  while (!filledEmail) {
    try {
      await (
        await driver.findElement(
          By.xpath('//*[@id="root"]/div/div/div/div[2]/form/div[1]/input')
        )
      ).sendKeys("anderson@email.com");

      filledEmail = true;
    } catch (error) {
      console.log(error);
      await driver.sleep(1000);
    }
  }

  let filledPassword = false;

  while (!filledPassword) {
    try {
      await (
        await driver.findElement(
          By.xpath('//*[@id="root"]/div/div/div/div[2]/form/div[2]/input')
        )
      ).sendKeys("1234");

      filledPassword = true;
    } catch (error) {
      console.log(error);
      await driver.sleep(1000);
    }
  }

  await driver.sleep(3000);

  let buttonLogin = false;

  while (!buttonLogin) {
    try {
      await (
        await driver.findElement(
          By.xpath('//*[@id="root"]/div/div/div/div[3]/button')
        )
      ).click();

      buttonLogin = true;
    } catch (error) {
      console.log(error);
      await driver.sleep(1000);
    }
  }

  await driver.sleep(3000);

  let buttonCalculator = false;

  while (!buttonCalculator) {
    try {
      await (
        await driver.findElement(
          By.xpath('//*[@id="root"]/div/div/div[2]/div/div/div/a[2]/button')
        )
      ).click();

      buttonCalculator = true;
    } catch (error) {
      console.log(error);
      await driver.sleep(1000);
    }
  }

  await driver.sleep(3000);

  for (const id of ids) {
    let hasId = false;

    while (!hasId) {
      try {
        await driver.wait(
          until.elementLocated(By.xpath('//*[@id="text-id"]')),

          60000
        );

        await (
          await driver.findElement(By.xpath('//*[@id="text-id"]'))
        ).clear();
        await (await driver.findElement(By.xpath('//*[@id="fees"]'))).clear();
        await (
          await driver.findElement(By.xpath('//*[@id="percentage"]'))
        ).clear();

        await driver.sleep(2000);

        for (const n of id) {
          await (
            await driver.findElement(By.xpath('//*[@id="text-id"]'))
          ).sendKeys(n);
        }

        hasId = true;
      } catch (error) {
        console.log(error);

        await driver.sleep(1000);
      }
    }

    let honorario = false;

    while (!honorario) {
      try {
        await (await driver.findElement(By.xpath('//*[@id="fees"]'))).sendKeys(
          "8"
        );

        honorario = true;
      } catch (error) {
        console.log(error);
        await driver.sleep(1000);
      }
    }

    let percentual = false;

    while (!percentual) {
      try {
        await (
          await driver.findElement(By.xpath('//*[@id="percentage"]'))
        ).sendKeys("10");

        percentual = true;
      } catch (error) {
        console.log(error);
        await driver.sleep(1000);
      }
    }

    let buttonCalcular = false;

    while (!buttonCalcular) {
      try {
        await (
          await driver.findElement(
            By.xpath("/html/body/div/div/div/div[3]/form[1]/h2[6]/button")
          )
        ).click();

        buttonCalcular = true;
      } catch (error) {
        console.log(error);

        await driver.sleep(1000);
      }
    }

    await driver.sleep(5000);
  }

  return "success";
}

export { teste };
